\include "../ĉiea.ly"

gamo = {
  s1 s1 s1 s1 b-2 c'1-3 d'1-0 |
  \circle e'-1 fis'-2 g'-3 a'-0 b'-1 c''-2 d''-3 |
}
gamohe = \lyricmode {סי דו רה | מי פה♯ סול לה סי דו רה |}
gamoeo = \lyricmode {B C D | E F♯ G A B C D |}

\include "../gamo.ly"

\header {
  title       = ""
  title-he    = "בת שישים"
  title-eo    = "Sesdek jara"
  composer    = ""
  composer-he = "קובי אשרת"
  composer-eo = "Kobi Oŝrat"
  gamo-he     = "מי מינור"
  gamo-eo     = "e-minoro"
  symbol      = "⛰"
}

\include "../titolo.ly"

melodio = {
  \key e \minor
  \time 4/4
  \tempo 4 = 120
  r4 d8 d d d d d       | 
  d4 g2 fis8 g          | 
  a4 g fis e8 c~        | 
  c1                    | 
  r4 c8 c c c c c       | 
  c4 a2 g8 fis          | 
  e4 d8 d~ d2           | 
  r1                    | 
  r4 b,8 b, b, b, b, b, | 
  b,4  g2 fis8 g        | 
  a4 gis a b8 a~        | 
  a2. e8 fis            | 
  a4 g fis e            | 
  g4 e b, e             | 
  cis2 dis2             | 
  e2 g4 fis8 g~         | 
  g4 e g fis8 g~        | 
  g4 c g g              | 
  fis4 b, e fis8 g~     | 
  g2. fis8 g            | 
  a4 g a g8 b~          | 
  b4 g d b              | 
  a2 fis                | 
  b1                    | 
  r4 e g a              | 
  b4 e2 g8 a            | 
  b4 e2 a8 b            | 
  c'4 b a b8 c'~        | 
  c'2. a8 b             | 
  c'4 b c' b8 d'~       | 
  d'4 c' b a8 c'~       | 
  c'4 b b a8 b~         | 
  b4 e g a              | 
  b4 e2 g8 a            | 
  b4 e2 a8 b            | 
  c'4 b d' c'8 c'~      | 
  c'2. a8 b             | 
  d'4 c' b a8 c'~       | 
  c'4 b g e8 b~         | 
  b4 a g fis8 g~        | 
  g2. a8 b              | 
  d'4 c' a fis8 c'~     |
  c'4 b g e             | 
  cis4 e dis fis        | 
  e1 \bar "|."
}

akordoj = \chordmode {
  \time 4/4
  g1*2       | 
  a1*2:m     | 
  d1*2:7     | 
  g1         | 
  b1:7       | 
  e1:m       | 
  e1:m/d     | 
  a1*2/cis   | 
  a1:m/c     | 
  e1:m       | 
  fis2:7 b:7 | 
  e1:m       | 
  e1:m       | 
  a1:m       | 
  b1         | 
  e1:m       | 
  a2:m d2    | 
  g1         | 
  fis1:m7    | 
  b1*2:7     | 
  e1*2:m     | 
  a1*2:m     | 
  d1*2       | 
  g1         | 
  b1         | 
  e1*2:m     | 
  a1*2:m     | 
  a1:m       | 
  e1:m       | 
  b1         | 
  e1:m       | 
  a1:m       |
  e1:m       | 
  fis2:7 b:7 | 
  e1:m       | 
}

\include "../muziko.ly"
