\include "../ĉiea.ly"

gamo = {
  s1 s1 s1 s1 s1 s1 c'1-3 |
  \circle d'1-0 e'-1 f'-2 g'-3 a'-0 bes'-1 c''-2 |
  d''-3
}

gamohe = \lyricmode {דו | רה מי פה סול לה סי♭ דו | רה}
gamoeo = \lyricmode {C | D E F G A B♭ C | D}

\include "../gamo.ly"

\header {
  title       = ""
  title-he    = "שיר לשלום"
  title-eo    = "Kanto por la Paco"
  composer    = ""
  composer-he = "יאיר רוזנבלום"
  composer-eo = "Jair Rozenblum"
  gamo-he     = "רה מינור"
  gamo-eo     = "d-minoro"
  symbol      = "☮"
}

\include "../titolo.ly"

melodio = {
  \key d \minor
  \time 4/4
  \repeat volta 2 {
    d4^"א׳+ב׳" a4 a4. a8 | 
    a4 g8 g4. r8 g8      | 
    c'4 bes4 a8 g4 a8~   | 
    a2. r4               | 
    f4 f4 f4. a8         | 
    a4 g8 g4. r8 f8      | 
  }
  \alternative {
    {
      f4 e4 g8 a4 f8~ | 
      f2. r4          | 
    }
    {
      f4 e4 g8 e4 d8~ | 
      d2. r4          | 
    }
  }
  c4^"ג׳" bes4 bes4. c'8   | 
  bes4 a8 a4. r8 c8        | 
  c4 bes4 bes8 c'4 a8~     | 
  a4. bes8 a8 g8 f8 e8     | 
  d4 a4 a4. a8             | 
  a4 g8 g4. r8 f8          | 
  f4 e4 g8 e4 d8~          | 
  d2 r8 d8^"ד׳" f8 a8      | 
  d'2 d'2                  | 
  d'8 c'16 bes16 c'2 r8 a8 | 
  bes4 a4 g8 d'4 a8~       | 
  a2 r8 d8 f8 a8           | 
  d'2 d'2                  | 
  d'8 c'16 bes16 c'2 r8 a8 | 
  bes4 a4 g8 f4 e8~        | 
  e2 a2^"D.C."             | 
  d1 \bar "|."
}

akordoj = { }

%melodio = {
%  \key e \minor
%  \time 4/4
%  \tempo 4 = 120
%  e4 b4 b4. c'8 |
%  b4 a8 a4. r8 a8 |
%  d'4 c'4 b8 a4 b8~ |
%  b2. r4 |
%  g4 g4 g4. b8 |
%  b4 a8 a4. r8 g8 |
%  g4 fis4 a8 b4 g8~ |
%  g2. r4 |
%}

\include "../muziko.ly"
