\score {
  <<
	 
    \new ChordNames {\override ChordNames.ChordName.font-name = #"Vesper Pro" \override ChordNames.ChordName.font-size = #-3 \akordoj}
	\new Staff \transpose c c' {\melodio}
  >>
}




\score {
  \new Staff \with {midiInstrument = #"cello"} {
	\unfoldRepeats	
	<< \transpose c c' {\melodio} \\ \transpose c' c, {\akordoj} >>
  }
  \midi { }
}
