% ⚙: http://web.nli.org.il/sites/NLI/Hebrew/music/Naomi_Shemer/Songs/LuYehi/03a.jpg

\include "../ĉiea.ly"

gamo = {
  \circle d'1 e' f' g' a' bes' c'' |
  d'' e'' f''
}
gamohe = \lyricmode{רה מי פה סול לה סי♭ דו | רה מי פה}
gamoeo = \lyricmode{D E F G A B♭ C | D E F}

\include "../gamo.ly"

\header {
  title       = ""
  title-he    = "לו יהי"
  title-eo    = "Estus"
  composer    = ""
  composer-he = "נעמי שמר"
  composer-eo = "Naomi Ŝemer"
  gamo-he     = "רה מינור"
  gamo-eo     = "d-minoro"
  symbol      = "🌟"
}

\include "../titolo.ly"

muziko_orig = {
  \key a \minor
  \time 4/4
  s2. s8 c'8            | 
  b8 a g f e a, d e     | 
  f8 f f f e d d4       | 
  c8 d e a, b,4 c8. c16 | 
  a,2. r8 c'8           | 
  b8 a g f e a, d e     | 
  f8 f f f e d d4       | 
  c8 d e a, b,4 c8. c16 | 
  a,2. c8 d             | 
  e4 e8 f g4 e8 c       | 
  a4. g8 f2             | 
  e8 f e d e4 b,8. b,16 | 
  c2. c8 d              | 
  e4 e8 f g4 e8 c       | 
  a4. g8 f2             | 
  e8 f e d e4 b,8. b,16 | 
  a,1               \bar "|."
}

melodio = {
  \key d \minor
  \time 4/4
  \tempo 4 = 120
  s2. s8 f'-1 |
  e'-0 d'-3 c'-2 bes-1 a-0 d-0 g-3 a-0 |
  bes-1 bes-1 bes-1 bes-1 a-0 g-3 g4-3 |
  f8-2 g-3 a-0 d-0 e4-1 f8.-2 f16-2 |
  d2.-0 r8 f'-1 |
  e'-0 d'-3 c'-2 bes-1 a-0 d-0 g-3 a-0 |
  bes-1 bes-1 bes-1 bes-1 a-0 g-3 g4-3 |
  f8-2 g-3 a-0 d-0 e4-1 f8.-2 f16-2 |
  d2.-0 f8-2 g-3 |
  a4-0 a8-0 bes-1 c'4-2 a8-0 f-2 |
  d'4.-3 c'8-2 bes2-1 |
  a8-0 bes-1 a-0 g-3 a4-0 e8.-1 e16-1 |
  f2.-2 f8-2 g-3 |
  a4-0 a8-0 bes-1 c'4-2 a8-0 f-2 |
  d'4.-3 c'8-2 bes2-1 |
  a8-0 bes-1 a-0 g-3 a4-0 e8.-1 e16-1 |
  d1-0 \bar "|."
}

akordoj = {}

\include "../muziko.ly"
