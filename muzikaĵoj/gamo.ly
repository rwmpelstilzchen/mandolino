\paper {
  oddFooterMarkup = \markup{\vspace #4
  \fill-line{
	\vcenter \center-column {
	  \tiny \override #'(font-name . "Rutz_OE Regular Pro") \fromproperty #'header:gamo-he
	  \tiny \override #'(font-name . "Vesper Pro") \fromproperty #'header:gamo-eo
	}
    \vcenter \scale #'(.8 . .8){
      \score {
        <<
          \new Staff = "a" {
            \clef "treble"
			\once \override Staff.TimeSignature #'stencil = ##f 
			\time 7/1
			\new Voice = "b" { \gamo }
		  }
			\new Lyrics \with { alignAboveContext = #"a" } \lyricsto "b" { \gamoeo }
			\new Lyrics \with { alignAboveContext = #"a" } \lyricsto "b" { \gamohe }

            \new TabStaff \with {
			  \override TabNoteHead #'font-name = #'"Century Schoolbook L Bold"
              stringTunings = #mandolin-tuning
            } {\gamo}
          >>
          \layout {
          }
        }
      }
    }
  }
}
