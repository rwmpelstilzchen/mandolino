\version "2.18.2"

#(set-global-staff-size 28)

\paper {
#(define fonts
   (make-pango-font-tree
	 "Rutz_OE Regular Pro"
	 "Alef"
	 "PragmataPro"
	 (/ staff-height pt 20)))
  indent = 0\cm
  paper-width = 18.5\cm
  tagline = ##f
  %ragged-bottom = ##f
  %ragged-last-bottom = ##f
}

circle = \once \override NoteHead.stencil = #(lambda (grob)
(let* ((note (ly:note-head::print grob))
	   (combo-stencil (ly:stencil-add
						note
						(circle-stencil note 0.1 0.3))))
  (ly:make-stencil (ly:stencil-expr combo-stencil)
				   (ly:stencil-extent note X)
				   (ly:stencil-extent note Y))))
