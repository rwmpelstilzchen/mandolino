\paper {
  bookTitleMarkup = {}

  scoreTitleMarkup = \markup { \column {
	\fill-line {
	  \line { \override #'(font-name . "Vesper Pro Bold") \fromproperty #'header:title-eo }
	  \line {
		\fontsize #2 \override #'(font-name . "Symbola") \fromproperty #'header:symbol
		\fontsize #2 \bold \fromproperty #'header:title
	  }
	  \line {
		\override #'(font-name . "Rutz_OE Bold Pro") \fromproperty #'header:title-he
	  }
	}
	\fill-line {
	  \line { \fontsize #-2 \fromproperty #'header:subtitle-eo }
	  \line { \fontsize #-1 \fromproperty #'header:subtitle }
	  \line { \override #'(font-name . "Rutz_OE Regular Pro") \fontsize #-2 \fromproperty #'header:subtitle-he }
	}
	\fill-line {
	  \line { \override #'(font-name . "Vesper Pro") \fromproperty #'header:composer-eo }
	  \line { \override #'(font-name . "Vesper Pro") \fromproperty #'header:composer }
	  \line { \override #'(font-name . "Rutz_OE Regular Pro") \fromproperty #'header:composer-he }
	}
  }
}


}
